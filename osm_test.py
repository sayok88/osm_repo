import overpy


class OSMNodeManager:
    def __init__(self, xml_file=''):
        self.api = overpy.Overpass()
        if xml_file:
            self._read_xml(xml_file)

    def _read_xml(self, file):
        with open(file, encoding="utf8") as f:  # 'kolkata_osm.xml'
            data = f.read()
        self.results = self.api.parse_xml(data)
        self.nodes = self.results.nodes
        self.relations = self.results.relations

    def load_relations(self):
        types = []

        for r in self.relations:
            if 'type' in r.tags:
                self.add_relation(r)
                if r.tags['type'] not in types:
                    types.append(r.tags['type'])

        self.relation_types = types

    def add_relation(self, relation):
        rel_list = getattr(self, 'rel_' + relation.tags['type'] + 's', None)
        if not rel_list:
            setattr(self, 'rel_' + relation.tags['type'] + 's', [])
            rel_list = getattr(self, 'rel_' + relation.tags['type'] + 's', None)
        rel_list.append(relation)

    def all_properties_nodes(self):
        property_list = list()
        for node in self.nodes:
            if node.tags:
                for prop_s in node.tags:
                    if prop_s not in property_list:
                        property_list.append(prop_s)
        return property_list

    def list_node_names(self):
        place_names = []
        for node in self.nodes:
            if node.tags and 'name' in node.tags:
                if 'name:en' in node.tags:
                    place_names.append(node.tags['name:en'])
                else:
                    place_names.append(node.tags['name'])
        return place_names

    def places_types(self):
        places = []
        for node in self.nodes:
            if node.tags and 'place' in node.tags:
                if node.tags['place'] not in places:
                    places.append(node.tags['place'])
        return places


if __name__ == '__main__':
    osr = OSMNodeManager(xml_file='data/kolkata_osm.xml')
    print(osr.list_node_names())
